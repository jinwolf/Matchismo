//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Jin Lee on 11/3/13.
//  Copyright (c) 2013 jinwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
