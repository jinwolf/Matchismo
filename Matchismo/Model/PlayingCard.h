//
//  PlayingCard.h
//  Matchismo
//
//  Created by Jin Lee on 11/10/13.
//  Copyright (c) 2013 jinwolf. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card
@property(strong, nonatomic) NSString* suit;
@property(nonatomic) NSUInteger rank;

+(NSArray *)validSuits;
+(NSUInteger)maxRank;
@end
