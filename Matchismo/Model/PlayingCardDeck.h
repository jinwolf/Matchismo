//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Jin Lee on 11/10/13.
//  Copyright (c) 2013 jinwolf. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
