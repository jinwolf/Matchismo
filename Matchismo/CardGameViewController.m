//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Jin Lee on 11/3/13.
//  Copyright (c) 2013 jinwolf. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "Playingcard.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) Deck *deck;
@property (strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel * scoreLabel;
@end

@implementation CardGameViewController

-(CardMatchingGame *) game {
    if (!_game) {
        _game = [[ CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                   usingDeck:[self createDeck]];
    }
    
    return _game;
}

- (IBAction)resetGame:(UIButton *)sender {
    
    _game = nil;
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score %d", self.game.score];
    
    [self updateUI];
}

-(Deck *)deck {
    if(!_deck) {
        _deck = [self createDeck];
    }
    
    return _deck;
}

-(Deck *)createDeck {
    return [[PlayingCardDeck alloc]init];
}

- (IBAction)touchCardButton:(UIButton *)sender {
    int chosenButtonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:chosenButtonIndex];
    [self updateUI];
}

- (void) updateUI {
    for (UIButton *cardButton in self.cardButtons) {
        int cardButtonIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardButtonIndex];
        [cardButton setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backgroundImageForCard:card] forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"Score %d", self.game.score];
    }
}

- (UIImage *)backgroundImageForCard:(Card *)card {
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}

- (NSString *) titleForCard:(Card *) card {
    return card.isChosen ? card.contents :@"";
}

- (void)setFlipCount:(int)flipCount {
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips %d", self.flipCount];
    NSLog(@"flipCount changed to %d", self.flipCount);
}
@end
